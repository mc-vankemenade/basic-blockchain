package block

import (
	"fmt"
	"crypto/sha256"
	"time"
	"strings"
)

type Block struct {
	BlockTime int64
	Nonce int
	Data []string
	PreviousHash string
	Hash string
}

func Genesis(data string) Block {
	b := Block{}
	b.BlockTime = time.Now().Unix()
	b.Nonce = 0
	b.Data = append(b.Data, data)
	b.PreviousHash = "0"
	b.Hash = ""

	return b
}

func New(data string, previousBlock Block) Block {
	b := Block{}
	b.BlockTime = time.Now().Unix()
	b.Nonce = 0
	b.Data = append(b.Data, data)
	b.PreviousHash = previousBlock.Hash
	b.Hash = ""

	return b
}

func (b Block) Mine(difficulty int) Block {
	hashPrefix := ""
	b.Hash = fmt.Sprintf("%x",sha256.Sum256([]byte(fmt.Sprintf("%#v", b))))
	for i:=1; i<=difficulty; i++ {
		hashPrefix = hashPrefix + "0"
	}
	for{
		if strings.HasPrefix(b.Hash, hashPrefix) {
			return b
		} else {
			b.Hash = ""
			b.Nonce += 1
			fmt.Println(b.Nonce)
			b.Hash = fmt.Sprintf("%x",sha256.Sum256([]byte(fmt.Sprintf("%#v", b))))
		}
	}
}
