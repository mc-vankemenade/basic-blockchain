package blockchain

import (
	"fmt"
	"blockchain/block"
	"crypto/sha256"
	"strings"
)

type blockChain struct {
	blocks []block.Block
	difficulty int
	pendingTransactions []string
	isRunning chan bool
	sendTransaction chan string
}

func New(difficulty int) blockChain {
	genesisBlock := block.Genesis("Lorem Ipsum")
	genesisBlock = genesisBlock.Mine(difficulty)

	bc := blockChain{}
	bc.difficulty = difficulty
	bc.blocks = append(bc.blocks, genesisBlock)
	bc.isRunning = make(chan bool)
	bc.sendTransaction = make(chan string)
	return bc
}

func (bc blockChain) Start() bool {
	go bc.mainLoop()
	bc.isRunning <- true
	return true
}

func (bc blockChain) Stop() bool {
	bc.isRunning <- false
	return true
}

func (bc blockChain) CreateTransaction(transaction string) bool {
	bc.sendTransaction <- transaction
	return true
}

func proofOfWork(newBlock block.Block, lastBlock block.Block, difficulty int) bool {
	hashPrefix := ""
	for i:=1; i<=difficulty; i++ {
		hashPrefix = hashPrefix + "0"
	}

	newBlockHash := newBlock.Hash
	newBlock.Hash = ""
	newBlock.Hash = fmt.Sprintf("%x",sha256.Sum256([]byte(fmt.Sprintf("%#v", newBlock))))

	if newBlockHash == newBlock.Hash && strings.HasPrefix(newBlock.Hash, hashPrefix) && lastBlock.Hash == newBlock.PreviousHash {
		fmt.Println("Work is valid!")
		return true
	} else {
		return false
	}
	
}

func (bc blockChain) mainLoop() {
	for true {
		select {
			default:
				if len(bc.pendingTransactions) > 0 {
					b := block.New(bc.pendingTransactions[0], bc.blocks[len(bc.blocks)-1])
					b = b.Mine(bc.difficulty)
					if proofOfWork(b, bc.blocks[len(bc.blocks)-1], bc.difficulty) {
						bc.pendingTransactions = append(bc.pendingTransactions[:0], bc.pendingTransactions[1:]...)
						bc.blocks = append(bc.blocks, b)
						fmt.Println("block added to chain: ")
						fmt.Println(b)
					} else {
						fmt.Println("Block Rejected")
					}
				}
			
			case transaction := <-bc.sendTransaction:
				bc.pendingTransactions = append(bc.pendingTransactions, transaction)
				fmt.Println("Transaction added to pool: " + transaction)
				
			case signal := <-bc.isRunning:
				if signal {
					fmt.Println("Starting blockchain...")
				} else {
					fmt.Println("Stopping blockchain...")
					return
				}
				
		}

	}
}