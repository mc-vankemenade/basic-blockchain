package main

import (
	"blockchain"
	"fmt"
	"bufio"
	"os"
)

func main() {
	blockchain := blockchain.New(6)	
	blockchain.Start()

	scanner := bufio.NewScanner(os.Stdin)

	fmt.Print("Please make a transaction: ")
	for{
		for scanner.Scan() {
			input := scanner.Text()
			blockchain.CreateTransaction(input)
		}
		
	}
}